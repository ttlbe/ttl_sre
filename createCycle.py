from jwtGenerator import *
import requests, json

class CreateCycle:

    def __init__(self,name, projectId,__userId, __accessKey, __secretKey,BASE_URL):
        self.name = name
        self.projectId = projectId
        self.__userId = __userId
        self.__accessKey = __accessKey 
        self.__secretKey = __secretKey
        self.BASE_URL = BASE_URL

    def make(self):
        
        # REQUEST PAYLOAD: to create cycle
        cycle = {
                    'name': self.name,
                    'projectId':  self.projectId,
                    'versionId': -1
                }

        # RELATIVE PATH for token generation and make request to api
        RELATIVE_PATH = '/public/rest/api/1.0/cycle'
        CANONICAL_PATH = 'POST&'+RELATIVE_PATH+'&'
        
        # MAKE REQUEST:

        jws = JWTGenerator(self.__userId, self.__accessKey, self.__secretKey, CANONICAL_PATH)

        raw_result = requests.post(self.BASE_URL + RELATIVE_PATH, headers=jws.headers , json=cycle)
        
        
        def is_json(data):
            try:
                json.loads(data)
            except ValueError:
                return False
            return True
            
        
        if is_json(raw_result.text):

            # JSON RESPONSE: convert response to JSON
            json_result = json.loads(raw_result.text)

            # PRINT RESPONSE: pretty print with 4 indent
            print(json.dumps(json_result, indent=4, sort_keys=True))

        else:
            print(raw_result.text)