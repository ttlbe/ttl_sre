from jwtGenerator import *
import requests, json

class GetAllTestSteps:

    def __init__(self,__userId,__accessKey,__secretKey,BASE_URL):
        self.__userId = __userId
        self.__accessKey = __accessKey
        self.__secretKey = __secretKey
        self.BASE_URL = BASE_URL

    def get(self, issueId, projectId):

        # RELATIVE PATH for token generation and make request to api
        RELATIVE_PATH = '/public/rest/api/1.0/teststep/'+issueId+'?projectId='+projectId
        self.CANONICAL_PATH = 'GET&'+RELATIVE_PATH+'&'
        
        # MAKE REQUEST:

        jws = JWTGenerator(self.__userId,self.__accessKey, self.__secretKey, self.CANONICAL_PATH)

        raw_result = requests.get(self.BASE_URL + RELATIVE_PATH, headers=jws.headers)
        
        
        def is_json(data):
            try:
                json.loads(data)
            except ValueError:
                return False
            return True
            
        
        if is_json(raw_result.text):

            # JSON RESPONSE: convert response to JSON
            json_result = json.loads(raw_result.text)

            # PRINT RESPONSE: pretty print with 4 indent
            print(json.dumps(json_result, indent=4, sort_keys=True))

        else:
            print(raw_result.text)